#include <highfive/H5Attribute.hpp>
#include <highfive/H5File.hpp>
#include <highfive/H5DataSet.hpp>
#include <highfive/H5DataSpace.hpp>
#include <QSettings>
#include <QVariant>

using namespace std;
using namespace HighFive;
class Data
{
public:
	struct Data_f
	{
		vector<int> years;
		vector<int> doy;
		vector<int> geo_id;
		vector<double> srad;
		vector<double> tmax;
		vector<double> tmin;
		vector<double> rain;
		vector<int> month;
		vector<double> dl;
		vector<string> clim_names = {"tmax", "tmin", "rain", "dl", "srad"};
		int nWeather;
		int nClCovar = 5;
	};
	Data_f data_h5;
	struct Data_a
	{
		vector<int> years;
		vector<int> doy;
		vector<int> geo_id;
		arma::vec response;
		vector<vector<double>> resp;
		vector<string> species;
		vector<int> month;
		vector<vector<double>> gr_covar;
		vector<string> gr_names;
		int nSamples;
		int nGrCovar;
	};
	Data_a data_a5;
	struct Data_m
	{
		int nResp;
		vector<vector<int>> years;
		vector<vector<int>> doy;
		vector<int> geo_id;
		vector<vector<double>> resp;
		vector<string> species;
		vector<vector<int>> month;
		vector<vector<double>> gr_covar;
		vector<string> gr_names;
		int nSamples;
		int nGrCovar;
	};
	Data_m data_m5;
	Data() {}
	void read_h5(QString file_name)
	{
		try {
            File file(file_name.toStdString(), File::ReadOnly);
			DataSet doy_read = file.getDataSet("doy");
			DataSpace space = doy_read.getSpace();
			data_h5.nWeather = space.getDimensions()[0];
			doy_read.read(data_h5.doy);
			DataSet rain_read = file.getDataSet("rain");
			rain_read.read(data_h5.rain);
			DataSet srad_read = file.getDataSet("srad");
			srad_read.read(data_h5.srad);
			DataSet tmax_read = file.getDataSet("tmax");
			tmax_read.read(data_h5.tmax);
			DataSet tmin_read = file.getDataSet("tmin");
			tmin_read.read(data_h5.tmin);
			DataSet year_read = file.getDataSet("year");
			year_read.read(data_h5.years);
			DataSet dl_read = file.getDataSet("dl");
			dl_read.read(data_h5.dl);
			DataSet month_read = file.getDataSet("month");
			month_read.read(data_h5.month);
			DataSet geo_id_read = file.getDataSet("geo_id");
			geo_id_read.read(data_h5.geo_id);
		}
		catch (Exception &err){
			std::cerr << err.what() << std::endl;
		}
	}
	void read_spieces(QString file_name, bool extra_covar)
	{
	    try {
		    File file(file_name.toStdString(), File::ReadOnly);

			DataSet doy_read = file.getDataSet("doy");
			DataSpace space = doy_read.getSpace();
			data_a5.nSamples = space.getDimensions()[0];
			doy_read.read(data_a5.doy);
			DataSet year_read = file.getDataSet("year");
			year_read.read(data_a5.years);
			DataSet month_read = file.getDataSet("month");
			month_read.read(data_a5.month);
			DataSet geo_id_read = file.getDataSet("geo_id");
			geo_id_read.read(data_a5.geo_id);
			DataSet b_read = file.getDataSet("response");
			space = b_read.getSpace();
			int ns = space.getDimensions()[0]; // number of samples
			int nn = space.getDimensions()[1]; // number of measurements
			b_read.read(data_a5.resp);
			assert(ns == data_a5.nSamples);
			assert(nn == 1);
			DataSet a1_read = file.getDataSet("species");
			a1_read.read(data_a5.species);
			data_a5.nGrCovar = 0;
			if (extra_covar) {
				DataSet c0_read = file.getDataSet("gr_covar");
				DataSpace space = c0_read.getSpace();
				data_a5.nGrCovar = space.getDimensions()[1]; // number of measurements
				c0_read.read(data_a5.gr_covar);
				DataSet c1_read = file.getDataSet("gr_names");
				c1_read.read(data_a5.gr_names);
			}
	    } catch (Exception& err) {
    		std::cerr << err.what() << std::endl;
		}
		data_a5.response = Data::std2arvec(data_a5.resp, data_a5.nSamples, 0);
	}
	void read_spieces(QString file_name, bool extra_covar, int nR)
	{
	    try {
		    File file(file_name.toStdString(), File::ReadOnly);
			DataSet doy_read = file.getDataSet("doy");
			DataSpace space = doy_read.getSpace();
			data_m5.nSamples = space.getDimensions()[0];
			data_m5.nResp = space.getDimensions()[1];
			assert(nR == data_m5.nResp);
			doy_read.read(data_m5.doy);
			DataSet year_read = file.getDataSet("year");
			year_read.read(data_m5.years);
			DataSet month_read = file.getDataSet("month");
			month_read.read(data_m5.month);
			DataSet geo_id_read = file.getDataSet("geo_id");
			geo_id_read.read(data_m5.geo_id);
			DataSet b_read = file.getDataSet("response");
			space = b_read.getSpace();
			int ns = space.getDimensions()[0]; // number of samples
			int nn = space.getDimensions()[1]; // number of measurements
			b_read.read(data_m5.resp);
			assert(ns == data_m5.nSamples);
			assert(nn == nR);
			DataSet a1_read = file.getDataSet("species");
			a1_read.read(data_m5.species);
			data_m5.nGrCovar = 0;
			if (extra_covar) {
				DataSet c0_read = file.getDataSet("gr_covar");
				DataSpace space = c0_read.getSpace();
				data_m5.nGrCovar = space.getDimensions()[1]; // number of measurements
				c0_read.read(data_m5.gr_covar);
				DataSet c1_read = file.getDataSet("gr_names");
				c1_read.read(data_m5.gr_names);
			}
	    } catch (Exception& err) {
    		std::cerr << err.what() << std::endl;
		}
	}
	void write_csv(QString out_file_name, arma::mat &Fpoints, int nFunctions, int nDays)
	{
		ofstream out;
		out.open(out_file_name.toStdString(), ios::app);
		out << "ID" << "," << "d" << ",";
		for (size_t n = 0; n < nFunctions; ++n) out << "I" << n << ",";
		for (size_t m = 0; m < data_a5.nGrCovar; ++m) out << data_a5.gr_names[m] << ",";
		out << "state" << ",";
		out << "nsam" << ",";
		out << "event_day" << endl;
		for (size_t nsam = 0; nsam < data_a5.nSamples; ++nsam) {
			for(size_t nd = 0; nd < nDays; ++nd) {
				out << nsam << "," << nd << ",";
				for (size_t n = 0; n < nFunctions; ++n) out << Fpoints(nDays * nsam + nd, n) << ",";
				for (size_t m = 0; m < data_a5.nGrCovar; ++m) out << data_a5.gr_covar[nsam][m] << ",";
				out << Fpoints(nDays * nsam + nd, nFunctions) << ",";
				out << Fpoints(nDays * nsam + nd, nFunctions + 1) << ",";
				out << Fpoints(nDays * nsam + nd, nFunctions + 2) << endl;
			}
		}
		out.close();
	}
private:
	arma::mat std2arvec(std::vector<std::vector<double> > &vec, int n_rows, int offset) {
		arma::vec Y(n_rows, 1);
		for (size_t i = 0; i < n_rows; ++i) {
			Y(i) = vec[i][offset];
		}
		return Y;
	}
};

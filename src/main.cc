/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * main.cc
 * Copyright (C) 2018 kkozlov <mackoel@gmail.com>
 *
 * nlreg is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * nlreg is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>
#include <iostream>
#include <QtCore>
#include <regex>

#ifdef INCL_OMP
#   #include <omp.h>
#endif
#include "task.h"

std::vector<int> splitString(const std::string& stringToSplit, const std::string& regexPattern){
	std::vector<int> result;
	int elem;
	const std::regex rgx(regexPattern);
	std::sregex_token_iterator iter(stringToSplit.begin(), stringToSplit.end(), rgx, -1);

	for (std::sregex_token_iterator end; iter != end; ++iter)
	{
		elem = std::stoi(iter->str());
		if(elem != 0){
			result.push_back(elem);
		}
	}

	return result;
}
		
int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);
	QCommandLineParser parser;
	QString description = "Programm impements grammatical evolution method.\n\n \
	List of available functions codes:\n \
	[1]  Add                  expression + expression\n \
	[2]  Subtract             expression - expression\n \
	[3]  Multiply             expression * expression\n \
	[4]  Divide               expression / expression\n \
	[5]  InputMinusConst      predictor - const\n \
	[6]  ConstMinusInput      const - predictor\n \
	[7]  RecInputMinusConst   1 / (predictor - const)\n \
	[8]  RecConstMinusInput   1 / (const - predictor)\n \
	[9]  InputMultConst       predictor * const\n \
	[10]  Sqr                  (expression)^2\n \
	[11]  Sqrt                 (expression)^(1/2)\n \
	[12] Sin                  sin(expression)\n \
	[13] Cos                  cos(expression)\n \
	[14] Log                  ln(expression)\n \
	[15] Exp                  exp(expression)\n \
	[16] Max                  max(expression, expression)\n \
	[17] Min                  min(expression, expression)\n \
	[18] WA                   const * pred1 + (1 - const) * pred2\n \
	[19] OWA                  const * max(pred1, pred2) + (1 - const) * min(pred1, pred2)";
	
	parser.setApplicationDescription(description);
	parser.addHelpOption();
	parser.addVersionOption();
	parser.addOptions({
/*        // A boolean option with a single name (-p)
	{"p",
	QCoreApplication::translate("main", "Show progress during copy")},*/
	// A boolean option with multiple names (-f, --force)
#ifdef INCL_OMP
	{{"k", "omp-num-threads"},
		QCoreApplication::translate("main", "Number of OMP threads."),
		QCoreApplication::translate("main", "M")},
#endif
	{{"n", "number-of-funcs"},
		QCoreApplication::translate("main", "Number of functions."),
		QCoreApplication::translate("main", "N")},
	{{"l", "length-of-word"},
		QCoreApplication::translate("main", "Length of a word to represent one function."),
		QCoreApplication::translate("main", "L")},
	{{"p", "print-trace"},
		QCoreApplication::translate("main", "Print trace."),
		QCoreApplication::translate("main", "P")},
	{{"q", "extra_covar"},
		QCoreApplication::translate("main", "Read binary covariates.")},
	{{"u", "use_func"},
		QCoreApplication::translate("main", "Which functions to use."),
		QCoreApplication::translate("main", "functions_codes")},
	{{"e", "exclude_func"},
		QCoreApplication::translate("main", "Which functions to exclude from usage."),
		QCoreApplication::translate("main", "functions_codes")},
    {{"r", "train_ratio"},
        QCoreApplication::translate("main", "Which ratio of data will be used for training."),
                QCoreApplication::translate("main", "R")},
    {{"s", "random_seed"},
            QCoreApplication::translate("main", "Random seed which is used for split the dataset."),
            QCoreApplication::translate("main", "RS")},
    });
	parser.addPositionalArgument("file", "The file to open.");
	parser.addPositionalArgument("funcsfile", "The file to read funcs.");
	parser.process(a);
#ifdef INCL_OMP
	if (parser.isSet("omp-num-threads")) {
		const QString mParameter = parser.value("omp-num-threads");
		omp_set_num_threads(mParameter.toInt());
//		cout << "Number of threads set to " << mParameter.toInt() << endl;
//		if (!qputenv("OMP_NUM_THREADS", QByteArray::number(mParameter.toInt()))) {
//			cout << "Number of threads coudn't be set!" << endl;
//		}
	}
#endif


    const QStringList args = parser.positionalArguments();
    // Task parented to the application so that it
    // will be deleted by the application.
	Task *task;
	
	if(args.size()) {
		QString file_name = args.at(0);
		QString funcs_file_name = args.at(1);

		if (parser.isSet("number-of-funcs") && parser.isSet("length-of-word")) {
			if (!(parser.isSet("use_func") && parser.isSet("exclude_func"))){

				std::vector<int> use_func, exclude_func;

                if(parser.isSet("use_func")){
                    use_func = splitString(parser.value("use_func").toStdString(), ",");
                }

				if(parser.isSet("exclude_func")){
					exclude_func = splitString(parser.value("exclude_func").toStdString(), ",");
				}



				const QString nfParameter = parser.value("number-of-funcs");
				const int nf = nfParameter.toInt();
				if (nf < 0) {
					std::cout << "Bad nf: " + nf;
				}
				const QString wlParameter = parser.value("length-of-word");
				const int wl = wlParameter.toInt();
				if (wl < 0) {
					std::cout << "Bad wl: " + wl;
				}
				const QString pParameter = parser.value("print-trace");
				const int p = pParameter.toInt();
				if (p < 0) {
					std::cout << "Bad p: " + p;
				}
                double r = 1;
                if (parser.isSet("train_ratio")) {
                    const QString rParameter = parser.value("train_ratio");
                    r = rParameter.toDouble();
                    if (r < 0.5 || r >= 1) {
                        std::cout << "Invalid ratio!";
                    }
                }
                unsigned int random_seed = 42;
                if (parser.isSet("random_seed")) {
                    const QString rParameter = parser.value("random_seed");
                    random_seed = rParameter.toInt();
                }
				task = new Task(file_name, funcs_file_name, nf, wl, parser.isSet("extra_covar"), p, r, random_seed, &a, &use_func, &exclude_func);
//                task = new Task(file_name, "", nf, wl, parser.isSet("extra_covar"), p, &a, &use_func, &exclude_func);

				set_print_trace(p);
		} else {
			std::cout << "Use only one option: use_func or exclude_func" << std::endl;
		}
		}
#ifdef INCL_OMP
		if (parser.isSet("omp-num-threads")) {

            const QString mParameter = parser.value("omp-num-threads");
			 omp_set_num_threads(mParameter.toInt());

		}
#endif
	}
    // This will cause the application to exit when
    // the task signals finished.
    QObject::connect(task, SIGNAL(finished()), &a, SLOT(quit()));

    // This will run the task from the application event loop.
		QTimer::singleShot(0, task, SLOT(run_wi()));

    return a.exec();
}
